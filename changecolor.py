import bpy 
from bpy import context
import builtins as __builtin__

def console_print(*args, **kwargs):
    for a in context.screen.areas:
        if a.type == 'CONSOLE':
            c = {}
            c['area'] = a
            c['space_data'] = a.spaces.active
            c['region'] = a.regions[-1]
            c['window'] = context.window
            c['screen'] = context.screen
            s = " ".join([str(arg) for arg in args])
            for line in s.split("\n"):
                bpy.ops.console.scrollback_append(c, text=line)

def print(*args, **kwargs):
    """Console print() function."""

    console_print(*args, **kwargs) # to py consoles
    __builtin__.print(*args, **kwargs) # to system console
    
mesh_owners = {}
ob = bpy.context.object # get the active object for example

for ob in bpy.data.objects:
    if ob.type == 'MESH':
        ob.active_material.use_nodes = True
        print(ob.name)
        print(ob.active_material.diffuse_color[0])
        color0 = ob.active_material.diffuse_color[0]
        color1 = ob.active_material.diffuse_color[1]
        color2 = ob.active_material.diffuse_color[2]
        color3 = ob.active_material.diffuse_color[3]
        if ob.active_material.cycles.id_data.node_tree != None:
            print('uyee0')
            for node in ob.active_material.cycles.id_data.node_tree.nodes:
                for nodeInput in node.inputs:
                    if nodeInput.type == 'RGBA':
                        nodeInput.default_value = (color0, color1, color2, color3)
                        nodeInput.default_value = (color0, color1, color2, color3)
                        nodeInput.default_value = (color0, color1, color2, color3)
                        print('uyee1')